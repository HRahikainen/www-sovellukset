/* Vue.js shopping list application
 * Harri Rahikainen
 */

// Component representing one list-item row of the table.
// Emits remove event on button click.
Vue.component('list-item', {
    template: `
        <li class="collection-item">
        <div class="valign-wrapper">
            <p style="word-break:break-all; width:100%;">{{ name }}</p>
            <i v-on:click="$emit(\'remove\')" class="secondary-content tiny material-icons" style="cursor:pointer;">clear</i>
        </div>
        </li>`,

    props: ['name']
})

// Vue instance
const app = new Vue({
    el: '#app',
    data: {
        title: 'Shopping list',
        newItem: '',    // Current value of input field "item"
        shoppingList: []
    },
    mounted() {
        // Get shopping list from LocalStorage when instance has been mounted on the 'el'.
        if (localStorage.getItem('shoppingList')) {
            try {
                // Parse array from string format.
                this.shoppingList = JSON.parse(localStorage.getItem('shoppingList'));
            } catch (e) {
                // Assume corrupt data and delete.
                localStorage.removeItem('shoppingList');
            }
        }
    },

    methods: {

        addItem() {
            this.shoppingList.push(this.newItem);
            this.newItem = '';
            this.saveList();
        },

        removeItem(index) {
            this.shoppingList.splice(index, 1);
            this.saveList();
        },

        validateInput() {
            // Only one or more letters, digits, dots or commas allowed
            // between start and end of the input string.
            const expr = /^[a-zA-Z0-9.,]+$/;
            const MAX_STR_LENGTH = 30;
            if (expr.test(this.newItem) && (this.newItem.length <= MAX_STR_LENGTH)) {
                this.addItem();
            }
        },

        saveList() {
            const stringifiedList = JSON.stringify(this.shoppingList);
            localStorage.setItem('shoppingList', stringifiedList);
        },

        removeItems() {
            this.shoppingList = [];
            this.saveList();
        }
    }
});