/* Ristinolla-peli
 * (Viikkotehtävät 2 ja 3, viikko 1; viikkotehtävä 3, viikko 2)
 * Harri Rahikainen
 */

// Data structures used to play tictactoe.
// Symbols: 0 - empty, 1 - Player 1, 2 - Player 2

const testGrid5 = [
    [1, 0, 2],
    [1, 2, 1],
    [2, 1, 2]
];

function addSymbol(grid, symbol, row, col) {
    // If empty, add symbol, return 1
    if (grid[row][col] === 0) {
        grid[row][col] = symbol;
        console.log(`Symbol ${symbol} was added to coords (${row}, ${col}).`);
        return 1;
    }
    // If not empty, return -1
    return -1;
}

// Test adding a symbol to empty cell and two occupied. 
console.log(addSymbol(testGrid5, 1, 0, 1));
console.log(addSymbol(testGrid5, 2, 1, 1));
console.log(addSymbol(testGrid5, 1, 1, 0));
console.log(testGrid5);

function hasLine(grid) {
    // Function returns 1, if tictactoe grid contains winning line, 0 otherwise.

    // Player symbols
    const symbols = [1, 2];

    const diag1 = [grid[0][0], grid[1][1], grid[2][2]];
    const diag2 = [grid[2][0], grid[1][1], grid[0][2]];

    for (let s of symbols) {
        for (let i = 0; i < 3; i++) {
            // Check row i
            if ((grid[i][0] === s) && (grid[i][1] === s) && (grid[i][2] === s)) {
                console.log('Line found: ' + 1);
                return 1;
            }

            // Check column i
            if ((grid[0][i] === s) && (grid[1][i] === s) && (grid[2][i] === s)) {
                console.log('Line found: ' + 1);
                return 1;
            }
        }

        // Check diagonals
        for (let arr of [diag1, diag2]) {
            // .every() returns true if all symbols in array match current player symbol.
            if (arr.every(sym => sym === s)) {
                console.log('Line found: ' + 1);
                return 1;
            }
        }
    }

    console.log('Line found: ' + 0);
    return 0;
}

function clearGrid(grid) {
    // Clear grid data structure elements back to zero.
    // Reset HTML table cell contents to zero.
    for (let i = 0; i < 3; i++) {
        for (let j = 0; j < 3; j++) {
            // If it's 1 or 2, return to 0.
            if (grid[i][j] !== 0) {
                document.getElementById(i + ' ' + j).innerHTML = '0';
                grid[i][j] = 0;
            }
        }
    }
}

function main() {
    // Construct the board cells with JS.
    const headerCell = document.getElementById('header-cell');
    const tableBody = document.getElementById('table-body');

    // Add cells with coordinates as ids.
    for (let i = 0; i < 3; i++) {
        let tableRow = document.createElement('tr');
        for (let j = 0; j < 3; j++) {
            let cell = document.createElement('td');
            cell.className = 'cell';
            cell.id = i + ' ' + j; // Coordinates separated by a space.
            cell.innerHTML = '0';
            tableRow.appendChild(cell);
        }
        tableBody.appendChild(tableRow);
    }

    // Game grid data structure.
    let grid = [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]
    ];

    let turnsPlayed = 0;
    let playerSymbol = 1;

    // Toggle player symbol every turn.
    const changeTurn = () => {
        playerSymbol = (playerSymbol === 1) ? 2 : 1;
        headerCell.innerHTML = 'P' + playerSymbol;
    };

    // Add event listeners to all 9 cells.
    const cells = document.querySelectorAll('.cell');
    cells.forEach(cell => {
        cell.addEventListener('click', e => {
            console.log('Clicked ' + e.target.id);
            let coords = e.target.id.split(/\s+/);
            let y = parseInt(coords[0]);
            let x = parseInt(coords[1]);

            if (addSymbol(grid, playerSymbol, y, x) === 1) {
                // Underlying data structure update is done. Show change.
                e.target.innerHTML = playerSymbol;
                turnsPlayed++;
                // Check for victory or draw and clear grid after 1 s.
                // When one game ends, the other player gets to start first.
                if (hasLine(grid)) {
                    alert(`Player ${playerSymbol} has won the game!`);
                    setTimeout(clearGrid, 1000, grid);
                    turnsPlayed = 0;
                } else if (turnsPlayed >= 9) {
                    alert("It's a draw!");
                    setTimeout(clearGrid, 1000, grid);
                    turnsPlayed = 0;
                }
                changeTurn();
            }
        });
    });

}

document.onload = main();